## 3. Récupération de la home

Afin de récupérer la home, on pourra effectuer un call graphQL multiplexé afin d'obtenir les différents éléments nécessaires à l'affichage de la liste des jeux. (On incluera - ou non - la liste des gameDays (allegros), des lottoWeeks (lottos), des shifumis (games(kind: SHIFUMI)), des all_ins (games(kind: ALL_IN)), etc.)

Ces différentes collections sont paginées en utilisant la best-practice de Facebook - en utilisant ce qu'ils appellent une "connection" cursor-based

Spec ici:
https://relay.dev/graphql/connections.html

Quelques exemples de queries pour gameDays, lottoWeeks, shifumis, all_ins ci-dessous. On pourra effectuer ces quatre query en un seul call en utilisant le multiplexing de graphql.

**gameDays**
```
query GameDays($first: Int, $after: String, $last: Int, $before: String, $beforeDate: ISO8601Date, $afterDate: ISO8601Date) {
  gameDays(first: $first, after: $after, last: $last, before: $before, beforeDate: $beforeDate, afterDate: $afterDate){
    edges {
      node {
        date
        allThemeRuns {
          id
          slug
          displayJackpot
          playersCount
          maxParticipationsCount
          tagline
          frequencyDescription
          featuredGameKind
          payload
          playStartsAt
          playEndsAt
          allSagas {
            id
            playStartsAt
            playEndsAt
            drawStartsAt
          }
        }
      }
      cursor
    }
    pageInfo {
      startCursor
      endCursor
      hasPreviousPage
      hasNextPage
    }
  }
}
```

**lottoWeeks**
```
query LottoWeeks($first: Int, $after: String, $last: Int, $before: String, $beforeDate: ISO8601Date, $afterDate: ISO8601Date) {
  lottoWeeks(first: $first, after: $after, last: $last, before: $before, beforeDate: $beforeDate, afterDate: $afterDate){
    edges {
      node {
        startDate
        endDate
        allThemeRuns {
            id
            slug
            displayJackpot
            playersCount
            maxParticipationsCount
            tagline
            frequencyDescription
            featuredGameKind
            payload
            playStartsAt
            playEndsAt
        }
      }
      cursor
    }
    pageInfo {
      startCursor
      endCursor
      hasPreviousPage
      hasNextPage
    }
  }
}
```

**shifumis**
```
query Games($first: Int!, $after: String, $last: Int, $before: String, $kind: StandaloneGameKindEnum!) {
  games(first: $first, after: $after, last: $last, before: $before, kind: $kind){
    edges {
      node {
        id
        kind
        allPrizes {
          amount
          kind
        }
        player {
          id
          userName
          firstName
          lastName
        }
      }
      cursor
    }
    pageInfo {
      startCursor
      endCursor
      hasPreviousPage
      hasNextPage
    }
  }
}
```

**all ins (un seul pour l'instant)**
```
query Games($first: Int!, $after: String, $last: Int, $before: String, $kind: StandaloneGameKindEnum!) {
  games(first: $first, after: $after, last: $last, before: $before, kind: $kind){
    edges {
      node {
        id
        kind
        allPrizes {
          amount
          kind
        }
      }
      cursor
    }
    pageInfo {
      startCursor
      endCursor
      hasPreviousPage
      hasNextPage
    }
  }
}
```
