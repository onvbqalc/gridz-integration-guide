## 8. Visionnage publicité

Afin de jouer un scratch, il faut récupérer l'id du scratch (après la récupération de la saga) et effectuer une `mutation createGameParticipation`

Exemple ci-dessous:

```
mutation CreateGameParticipation($gameId: ID!, $participation: ParticipationInputObject!) {
  createGameParticipation(gameId: $gameId, participation: $participation) {
    participation {
      id
    }
    errors {
      path
      message
      extensions {
        code
      }
    }
  }
}
```

Exemple de variables:
```
{
  "gameId": "{{video_ad_id}}",
  "participation": {
    "turns": [
      { "action": "WATCH" }
    ]
  }
}
```
