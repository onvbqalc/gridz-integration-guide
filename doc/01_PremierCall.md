## 1. Le premier call: ouverture de l'app pour la première fois sur un device

Le premier call s'effectue après création d'un joueur lors de la première ouverture de l'app.

Firebase Authentication, une fois ce joueur créé, permet d'effectuer des requêtes authentifiées en ajoutant un Firebase ID Token:

https://firebase.google.com/docs/auth/admin/verify-id-tokens#retrieve_id_tokens_on_clients

Effectuer alors un call au endpoint de l'API de gridz (préproduction: https://api-staging-v3t7ec6k.gridzgame.com/graphql) en plaçant cet ID token dans le header `Authorization`. Exemple:

`Authorization: Bearer {{YOUR_FIREBASE_ID_TOKEN_GOES_HERE}}`.

Tous les calls à l'API doivent être effectués sur `/graphql`. Le premier call d'enregistrement du joueur du client auprès de l'API doit être une mutation GraphQL: `mutation createSelf`

En voici un exemple:

*Mutation*
```
mutation CreateSelf($player: PlayerInputObject!, $socialRole: SocialRoleEnum!) {
  createSelf(player: $player, socialRole: $socialRole) {
    player {
      id
      firstName
      lastName
      coins
      bills
      countryCode
      socialRole
      thanksCount
      cheerupsCount
      congratulationsCount
      sentThanksCount
      sentCheerupsCount
      sentCongratulationsCount
      email
      uuid
      lottosCount
      allegrosCount
      chequesCount
      scratchesCount
      shifumisCount
      sponsorCode
      mostPlayedNumbers
      isFollower
      isFollowee
      rewards(first: 10) {
        edges {
          node {
            amount
            kind
          }
          cursor
        }
        pageInfo {
          startCursor
          endCursor
          hasPreviousPage
          hasNextPage
        }
      }
      games {
        edges {
          node {
            id
          }
          cursor
        }
        pageInfo {
          startCursor
          endCursor
          hasPreviousPage
          hasNextPage
        }
      }
      followers(first: 10) {
        edges {
          node {
            id
            firstName
          }
        }
      }
      followees(first: 10) {
        edges {
          node {
            id
            firstName
          }
        }
      }
      achievements(first: 10) {
        edges {
          node {
            level
            sagaThemeSlug
          }
          cursor
        }
        pageInfo {
          startCursor
          endCursor
          hasPreviousPage
          hasNextPage
        }
      }
    }
    lottoWeeks {
      edges {
        node {
          startDate
          endDate
          allThemeRuns {
            slug
            displayJackpot
            playersCount
            maxParticipationsCount
            tagline
            frequencyDescription
            featuredGameKind
            payload
            playStartsAt
            playEndsAt
            allSagas {
                id
                playStartsAt
                playEndsAt
                drawStartsAt
                allGames {
                    id
                    kind
                    maxParticipationsCount
                    myRemainingParticipationsCount
                    allMyParticipations {
                        allRewards {
                            amount
                            kind
                        }
                    }
                }
            }
          }
        }
        cursor
      }
      pageInfo {
        startCursor
        endCursor
        hasPreviousPage
        hasNextPage
      }
    }
    gameDays {
      edges {
        node {
          date
          allThemeRuns {
            id
            slug
            displayJackpot
            playersCount
            maxParticipationsCount
            tagline
            frequencyDescription
            featuredGameKind
            payload
            playStartsAt
            playEndsAt
            allSagas {
              id
              playStartsAt
              playEndsAt
              drawStartsAt
            }
          }
        }
        cursor
      }
      pageInfo {
        startCursor
        endCursor
        hasPreviousPage
        hasNextPage
      }
    }
    errors {
      path
      message
      extensions {
        code
      }
    }
  }
}
```

*Variables*
```
{
  "socialRole": "SEEDED",
  "player": {
    "firstName": "{{first_name}}",
    "lastName": "{{last_name}}",
    "countryCode": "{{country}}",
    "userName": "{{user_name}}",
    "timeZone": "{{time_zone}}",
    "locale": "{{locale}}"
  }
}
```

À noter: le socialRole envoyé est `SEEDED` afin de séparer les joueurs de tests du reste des joueurs. Ce call doit inclure le `countryCode`, `timeZone` et `locale` du joueur afin que l'API puisse renvoyer le contenu correspondant à la situation géographique du joueur (lots dans les bonnes devises, lottos à 19h, etc.)

Exemple de `locale`: `EN_US`

Exemple de `countryCode`: `FR`

Exemple de `timeZone`: `EUROPE_PARIS`

La mutation renvoit des lottoWeeks et des gameDays, ainsi que des infos joueurs.

**À noter:** Il faudra ajouter toutes les infos nécessaires à l'affichage de la home et du profil joueur simplifié (p-e que son nom), et enlever tout ce qui n'est pas nécessaire à l'affichage du premier écran afin d'alléger au maximum la charge sur le back-end.

