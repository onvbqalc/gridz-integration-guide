## 5. Récupération d'une saga d'allegro

Une fois que le joueur a ouvert un theme run particulier, il clique sur une saga pour en afficher le contenu détaillé - notamment la séquence de jeu, c'est à dire l'ensemble des jeux auxquels il est possible de joueur au sein de la saga, et dans quel ordre.

E.g. allegro, allegro, pub, scratch, allegro, pub, allegro, cheque

Cette séquence est dynamique et inclue dans chaque saga. Pour la récupérer, on peut effectuer le call ci-dessous:

```
query Saga($sagaId: ID!) {
    saga(id: $sagaId) {
        theme {
            slug
            payload
        }
        allGames {
            id
            kind
            allPrizes {
                rank
                kind
                amount
            }
            myRemainingParticipationsCount
            maxParticipationsCount
            allMyParticipations {
                allRewards {
                    amount
                    kind
                }
            }
        }
        allMySequences {
            allFrames {
                gameId
                gridsWonCount
                isPlayable
                kind
                state
            }
        }
        announcementAt
        playStartsAt
        playEndsAt
        drawStartsAt
        playersCount
    }
}
```

*L'id est l'id récupéré pour une saga donnée lors de l'affichage du theme_run*

La séquence de jeu contient des frames, qui sont les étapes d'une saga.
