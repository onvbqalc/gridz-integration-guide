## 9. Enregistrement Device

Pour pouvoir envoyer des notifications aux joueurs, il faut récuperer le device token. Après avoir obtenu ce token de la part du joueur, il faut le transmettre au back-end avec le call ci-dessous

Exemple ci-dessous:

```
mutation CreateDevice($device: DeviceInputObject!) {
  createDevice(device: $device) {
    device {
      id
      os
      token
      player {
        id
      }
    }
    errors {
      path
      message
      extensions {
        code
      }
    }
  }
}
```

Exemple de variables:
```
{
  "device": {
    "os": "IOS",
    "token": "{{device_token}}"
  }
}
```
