Guide d'intégration

Objectif du guide

Périmètre et modélisation
  I. 3 grands Domaines
    A. Les joueurs
      Follow, unfollow
      Enregistrement de mon device
      Envoi de félicitations/remerciements/encouragements
      Structure détaillée du profil joueur
    B. Le jeu
      theme runs
      lotto
      sagas, games, participations
      crates, rewards
      Inscription à une saga future
    C. Le catalogue
      Structure détaillée du catalogue

GraphQL
  Considérations générales
  Le schema


Authentification

Notifications

La routine hebdomadaire

L'intégration
1. Le premier call: ouverture de l'app pour la première fois sur un device
2. Récupération du joueur en cours
3. Récupération de la home
4. Récupération d'un themeRun d'Allegro
5. Recupération d'une saga Allegro dans le theme run récupéré
6. Jeu d'une grille d'allegro
7. Jeu d'un scratch
8. Visionnage d'une pub d'allegro
9. Enregistrement de son device
10. Réception de la notification de tirage
11. Récupération des shifumis
12. Jeu d'un shifumi
13. Visite du profil d'un joueur (créateur du shifumi)
14. Follow d'un joueur (créateur du shifumi)
15. Récupération des thèmes de shifumi (pour création)
16. Création d'un shifumi
17. Récupération des lottos
18. Récupération d'un themeRun de lotto
19. Récupération d'une saga Lotto dans le thème run récupéré
20. Jeu d'une grille de lotto
21. Visionnage d'une pub de lotto
22. Récupération des activités récentes (simulation temps réel)
23. Récupération des joueurs vus récemment (simulation temps réel)
24. Réception de la notification de gain tirage
25. Récupération de mon profil pour voir les dernières grilles jouées
26. Récupération de mes crates en vue de les ouvrir
27. Ouverture de mes crates pour récupérer mes gains
28. Récupération du catalogue pour voir ce que je peux acheter
29. Achat d'un produit - échec car pas assez de billets
30. Conversion de mes pièces en billets
31. Achat d'un produit - échec car pas d'email rensigné sur mon profil
32. Mise à jour de mon profil pour y ajouter un email, nom, prénom, surnom
33. Achat d'un produit - succès car j'ai billets + email
34. Réception de l'email de commande
35. Récupération de mes commandes
36. Unfollow du joueur que j'avais follow
37. Inscription à une saga future
38. Réception de la notification d'ouverture d'une saga où je suis inscrit
39. Envoi d'un cheerup/thank/congratulation
40. Réception d'un cheerup/thank/congratulation
41. Changement de la langue de Gridz
42. Changement du fuseau horaire de Gridz
43. Changement de mon pays sur Gridz
44. Upload de mon avatar (direct firestore)
45. Ajout du code parrain de mon sponsor
