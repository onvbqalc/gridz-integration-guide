## 7. Jeu scratch

Afin de jouer un scratch, il faut récupérer l'id du scratch (après la récupération de la saga) et effectuer une `mutation createGameParticipation`

Exemple ci-dessous:

```
mutation CreateGameParticipation($gameId: ID!, $participation: ParticipationInputObject!) {
  createGameParticipation(gameId: $gameId, participation: $participation) {
    participation {
      id
      isWon
      allFees {
        kind
        amount
      }
      allTurns {
        action
      }
      game {
        id
      }
      allRewards {
        amount
        kind
      }
      player {
        id
        coins
        crates {
          edges {
            node {
              kind
              allRewards {
                amount
                kind
              }
              state
            }
          }
        }
      }
    }
    errors {
      path
      message
      extensions {
        code
      }
    }
  }
}
```

Exemple de variables:
```
{
  "gameId": "{{scratch_id}}",
  "participation": {
    "turns": [
      { "action": "LEFT_SWIPE" },
      { "action": "LEFT_SWIPE" },
      { "action": "LEFT_SWIPE" },
      { "action": "RIGHT_SWIPE" }
    ]
  }
}
```
