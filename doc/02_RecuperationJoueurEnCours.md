## 2. Récupération du joueur en cours

Afin d'afficher l'interface de GRIDZ, il est nécessaire de récupérer certaines infos du joueur en cours. Pour ce faire, on pourra utiliser la query ci-dessous:

```
query ShowSelf {
  showSelf {
    id
    firstName
    lastName
    coins
    bills
    countryCode
    socialRole
    thanksCount
    cheerupsCount
    congratulationsCount
    sentThanksCount
    sentCheerupsCount
    sentCongratulationsCount
    email
    uuid
    lottosCount
    allegrosCount
    chequesCount
    scratchesCount
    shifumisCount
    sponsorCode
    mostPlayedNumbers
    socials(first: 10) {
      edges {
        node {
          __typename
          ... on Cheerup {
            amount
            isSent
            isReceived
            createdAt
          }
          ... on Congratulation {
            amount
            isSent
            isReceived
            createdAt
          }
          ... on Thank {
            amount
            isSent
            isReceived
            createdAt
          }
        }
      }
    }
    rewards(first: 10) {
      edges {
        node {
          amount
          kind
        }
        cursor
      }
      pageInfo {
        startCursor
        endCursor
        hasPreviousPage
        hasNextPage
      }
    }
    playedGrids(last: 10) {
      edges {
        node {
          id
          numbers
          kind
          isWon
        }
        cursor
      }
      pageInfo {
        startCursor
        endCursor
        hasPreviousPage
        hasNextPage
      }
    }
    followers(first: 10) {
      edges {
        node {
            id
            firstName
        }
      }
    }
    followees(first: 10) {
      edges {
        node {
            id
            firstName
        }
      }
    }
    sponsor {
      id
      firstName
    }
    sponsorees(first: 10) {
      edges {
        node {
          id
          firstName
        }
      }
    }
    achievements(first: 10) {
      edges {
        node {
          level
          sagaThemeSlug
        }
        cursor
      }
      pageInfo {
        startCursor
        endCursor
        hasPreviousPage
        hasNextPage
      }
    }
  }
}
```

Cette query renvoit énormément d'informations. En fonction des besoins du front, il faudra retirer toute l'information qui n'est pas nécessaire à l'affichage du profil joueur.
