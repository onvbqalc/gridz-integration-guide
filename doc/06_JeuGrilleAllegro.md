## 6. Jeu grille allegro

Afin de jouer une grille à un allegro, il faut récupérer l'id de l'allegro (après la récupération de la saga) et effectuer une `mutation createGameParticipation`

Exemple ci-dessous:

```
mutation CreateGameParticipation($gameId: ID!, $participation: ParticipationInputObject!) {
  createGameParticipation(gameId: $gameId, participation: $participation) {
    participation {
      id
      isWon
      allFees {
        kind
        amount
      }
      allTurns {
        action
      }
      game {
        id
      }
      allRewards {
        amount
        kind
      }
      player {
        id
        coins
        crates {
          edges {
            node {
              kind
              allRewards {
                amount
                kind
              }
              state
            }
          }
        }
      }
    }
    errors {
      path
      message
      extensions {
        code
      }
    }
  }
}
```

Exemple de variables:
```
{
  "gameId": "{{allegro_id}}",
  "participation": {
    "turns": [
      { "action": "ALLEGRO_N_12" },
      { "action": "ALLEGRO_N_11" },
      { "action": "ALLEGRO_N_6" },
      { "action": "ALLEGRO_N_9" },
      { "action": "ALLEGRO_N_4" },
      { "action": "ALLEGRO_N_3" },
      { "action": "ALLEGRO_N_10" }
    ]
  }
}
```
