## 4. Récupération d'un theme run d'allegros

Une `saga` est un ensemble de jeux qui fonctionnent ensemble. Une `saga` d'allegro contient typiquement un jeu de la grille d'Allegro, un jeu de grattage, un visionnage de pub (traité comme un jeu aussi), et un jeu du chèque, pour 4 jeux au total qui fonctionnent de concert.

Un `theme_run` est une suite de saga qui ont le même thème. Pour une saga d'allegro toutes les 10 minutes et un thème lunch tonic de 12h à 14h, cela fera un theme_run de 12 sagas (2 heures, 6 sagas par heure)

Récupérer l'ensemble des participations d'un joueur à l'ensemble des jeux de l'ensemble des sagas de tous les theme_runs d'une journée demande trop d'effort au back-end. On demandera donc seulement la liste des sagas pour la home mais sans s'intéresser aux jeux qu'elles contiennent. Ensuite, on pourra ouvrir un theme_run en particulier et demander plus de détails au back-end (tous les jeux présents dans le theme_run et toutes les participations du joueurs dans ces jeux). Exemple ci-dessous:

```
query ThemeRun($id: ID!) {
  themeRun(id: $id) {
    slug
    displayJackpot
    playersCount
    maxParticipationsCount
    tagline
    frequencyDescription
    featuredGameKind
    payload
    playStartsAt
    playEndsAt
    allSagas {
      id
      playStartsAt
      playEndsAt
      drawStartsAt
      allGames {
        id
        kind
        maxParticipationsCount
        allMyParticipations {
          allRewards {
            amount
            kind
          }
        }
      }
    }
  }
}
```

*L'id est l'id récupéré pour un theme_run donné lors de l'affichage de la home*
